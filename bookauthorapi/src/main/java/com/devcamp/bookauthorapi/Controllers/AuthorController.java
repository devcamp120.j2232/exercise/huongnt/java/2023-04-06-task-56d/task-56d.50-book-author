package com.devcamp.bookauthorapi.Controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.bookauthorapi.models.Author;
import com.devcamp.bookauthorapi.service.AuthorService;
@CrossOrigin
@RestController
@RequestMapping("/api")

public class AuthorController {
    @Autowired


    private AuthorService authorService;

    @GetMapping("/region-info")
    public Author GetRegionInfo(@RequestParam(name="email", required = true) String email){
        Author findAuthor = authorService.getAuthorByEmail(email);
        return findAuthor;
    }
    
    @GetMapping("/author-gender")
    public ArrayList<Author> GetAuthorGender(@RequestParam(name="gender", required = true) char gender){
        ArrayList<Author> findAuthorGender = authorService.getAuthorByGender(gender);
        return findAuthorGender;
    }
}
