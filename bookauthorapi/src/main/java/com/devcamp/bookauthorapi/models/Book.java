package com.devcamp.bookauthorapi.models;

import java.util.ArrayList;

public class Book {
    private String name;
    private ArrayList<Author> authorlist;
    private double price;
    private int qty = 0;

    
    
    public Book() {
    }


    public Book(String name, ArrayList<Author> author, double price) {
        this.name = name;
        this.authorlist = author;
        this.price = price;
    }


    public Book(String name, ArrayList<Author> author, double price, int qty) {
        this.name = name;
        this.authorlist = author;
        this.price = price;
        this.qty = qty;
    }


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public ArrayList<Author> getAuthorlist() {
        return authorlist;
    }


    public void setAuthorlist(ArrayList<Author> authorlist) {
        this.authorlist = authorlist;
    }



    public double getPrice() {
        return price;
    }


    public void setPrice(double price) {
        this.price = price;
    }


    public int getQty() {
        return qty;
    }


    public void setQty(int qty) {
        this.qty = qty;
    }



    
}
